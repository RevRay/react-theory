import React from 'react'

const style = {
    backgroundColor: '#aaffaa',
    borderStyle: 'solid',
    borderColor: '#88bb88',
    borderRadius: '8px',
    borderWidth: '3px',
    margin: '1em',
    padding: '1em'
};

export default (props) => (
    <div style={style}>
        <h3>Car name: {props.name}</h3>
        <p>Year: <strong>{props.year}</strong></p>
        <button onClick={props.onChangeTitle}>Click</button>
    </div>
)